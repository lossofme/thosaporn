#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <math.h>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <turtlesim/Velocity.h>

float scale_linear_ = 0.2;
float scale_angular_ = 0.7;

float x;
float y;
float z;

//geometry_msgs::Twist vel;
turtlesim::Velocity vel;

void Callback(const sensor_msgs::Joy& msg)
{ 
	vel.linear = msg.axes[1] * scale_linear_;
	vel.angular = msg.axes[0] * scale_angular_;
}

int main(int argc, char** argv)
{   
    ros::init(argc, argv, "joy_to_sim");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("joy",100, &Callback); 
    ros::Publisher chatter_pub  = n.advertise<turtlesim::Velocity>("gg",100);
    //ros::Publisher chatter_pub  = n.advertise<turtlesim::Velocity>("/turtle1/command_velocity",100);
    ros::Rate rate(100);
    while(ros::ok())
{ 
    chatter_pub.publish( vel );
    ros::spinOnce();
    rate.sleep();
}	
  return 0;
}

