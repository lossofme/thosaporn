#include <ros/ros.h>
#include <tf/transform_listener.h>
float posx;
float posy;
float anglez;
float anglew;


int main(int argc, char** argv){
  ros::init(argc, argv, "my_tf_listener");

  ros::NodeHandle node;


  tf::TransformListener listener;

  ros::Rate rate(10.0);
  while (node.ok()){
    tf::StampedTransform transform;
    try{
      listener.lookupTransform("/base_link", "/odom",
                               ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
    }
    posx=transform.getOrigin().x();
    posy=transform.getOrigin().y();
    anglez=transform.getRotation().z();
    anglew=transform.getRotation().w();
    ROS_INFO("x:%f  y:%f  z:%f  w:%f",posx,posy,anglez,anglew);
    rate.sleep();
  }
  return 0;
};
