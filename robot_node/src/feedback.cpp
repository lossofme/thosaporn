#include <ros/ros.h>
#include <math.h>
#include <dynamixel_msgs/JointState.h>
#include <geometry_msgs/Twist.h>

#define PI 3.1415926535897932384626433
#define L1 0.16  
#define L2 0.145
#define R 0.076

double OmegaWheel1;
double OmegaWheel2;
double OmegaWheel3;
double OmegaWheel4;

void Callback(const dynamixel_msgs::JointState& msg)
{ 
    OmegaWheel1=msg.velocity;
}
void Callback2(const dynamixel_msgs::JointState& msh)
{ 
    OmegaWheel2=msh.velocity;
}
void Callback3(const dynamixel_msgs::JointState& msj)
{ 
    OmegaWheel3=msj.velocity;
}
void Callback4(const dynamixel_msgs::JointState& msk)
{ 
    OmegaWheel4=msk.velocity;
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "feedback");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("/wheel1/state", 1000, &Callback);
  ros::Subscriber sub2 = n.subscribe("/wheel2/state", 1000, &Callback2);
  ros::Subscriber sub3 = n.subscribe("/wheel3/state", 1000, &Callback3);
  ros::Subscriber sub4 = n.subscribe("/wheel4/state", 1000, &Callback4);
  ros::Publisher pub = n.advertise<geometry_msgs::Twist>("cmd_vel_feedback", 50);
  ros::Rate rate(100);
  geometry_msgs::Twist vel;
  while(n.ok())
    {
	vel.linear.x = ((OmegaWheel1*2) + (-2*OmegaWheel2) + (OmegaWheel3*2) + (-2*OmegaWheel4))*R/-1.8;
	vel.linear.y = ((OmegaWheel1*2) + (OmegaWheel2*2) + (-2*OmegaWheel3) + (-2*OmegaWheel4))*R/1.8;
	vel.angular.z = ((-2*OmegaWheel1/(L1+L2)) + (-2*OmegaWheel2/(L1+L2)) + (-2*OmegaWheel3/(L1+L2)) + (-2*OmegaWheel4/(L1+L2)))*R/-1.8;
	pub.publish(vel);

    //omegaWheel1.data = (velocity_x - velocity_y - (angular_z*(L1+L2)))/R/2.4;
    //omegaWheel2.data = -1*(velocity_x + velocity_y + (angular_z*(L1+L2)))/R/2.4;
    //omegaWheel3.data = (velocity_x + velocity_y - (angular_z*(L1+L2)))/R/2.4;
    //omegaWheel4.data = -1*(velocity_x - velocity_y + (angular_z*(L1+L2)))/R/2.4;


 	ros::spinOnce();
  	rate.sleep();
    }
  return 0;
  
}
