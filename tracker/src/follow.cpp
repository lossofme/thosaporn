#include <ros/ros.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <math.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>

#define PI 3.14159265
float z;
float theta;
float order;
int left=0;
int right=0;
int front=0;
float obstaclex[500];
float obstacley[500];
int i;
int check=1;
float delta;

void ZCallback(const std_msgs::Float64::ConstPtr& msg)
{ 
	z=msg->data;
}

void thetaCallback(const std_msgs::Float64::ConstPtr& mss)
{ 
	theta=mss->data;
}

void LaserCallback(const sensor_msgs::LaserScanConstPtr& msv)
{ 
for(i=230;i<270;i++)
	{
    obstaclex[i] = msv->ranges[i]*cos(i*0.36*PI/180);
    obstacley[i] = msv->ranges[i]*sin(i*0.36*PI/180);
	if(obstacley[i]< 0.4 && obstacley[i] > 0.1)
		{
		front=1;
		//ROS_INFO("obstaclexxxx");
		}
	else{}
	}
for(i=50;i<214;i++)
	{
	if(msv->ranges[i] < 0.2 && msv->ranges[i] > 0.1)
		{
		left=1;
		}
	}
for(i=286;i<500;i++)
	{
	if(msv->ranges[i] < 0.2 && msv->ranges[i] > 0.1)
		{
		right=1;
		}
	}
	delta=(msv->ranges[0]*cos(i*0.36*PI/180))-(msv->ranges[30]*cos(i*0.36*PI/180));
	//ROS_INFO("a %f   b %f  delta %f",msv->ranges[0]*cos(i*0.36*PI/180),msv->ranges[30]*cos(i*0.36*PI/180),fabs(delta));
}

void OrderCallback(const std_msgs::Int64::ConstPtr& msf)
{ 
    order=msf->data;
}


int main(int argc, char** argv)
{   
    ros::init(argc, argv, "Follow");
    ros::NodeHandle n;
    ros::Subscriber positionz_sub = n.subscribe("positionz",100, &ZCallback); 
    ros::Subscriber theta_sub = n.subscribe("theta",100, &thetaCallback); 
    ros::Subscriber scan_sub = n.subscribe("scan",100, &LaserCallback); 
    ros::Subscriber sub5 = n.subscribe("order",100, &OrderCallback); 
    ros::Publisher vel_pub  = n.advertise<geometry_msgs::Twist>("cmd_vel",100);
    geometry_msgs::Twist vel;
    ros::Rate rate(10);
    while(ros::ok())
{ 

if(order == 65 || order == 49 || order == 50 || order == 51|| order == 52|| order == 53)
{


	if(front==1 )
   	{
		
   	}
	else  if(front==0 )
	{
		if(z>0.8)
   		{
  			if(theta>3)
			{
			vel.linear.x=(0.9*z)-0.72;
			//vel.linear.x=sqrt(0.405*(z-0.8));
			vel.linear.y=0;
			vel.angular.z=((0.0316*theta)-0.0947)*-1;
			//ROS_INFO("1");
			vel_pub.publish(vel);
			}
			else if(theta<3 && theta>-3)
			{
			vel.linear.x=(0.9*z)-0.72;
			//vel.linear.x=sqrt(0.405*(z-0.8));
			vel.linear.y=0;
			vel.angular.z=0;
			//ROS_INFO("2");
			vel_pub.publish(vel);
			check=1;
			}
			else if(theta<-3)
			{
			vel.linear.x=(0.9*z)-0.72;
			//vel.linear.x=sqrt(0.405*(z-0.8));
			vel.linear.y=0;
			vel.angular.z=((0.0316*theta)+0.0947)*-1;
			//ROS_INFO("3");
			vel_pub.publish(vel);
			}
		}
		else
   	 	{
			vel.linear.x=0;
			vel.linear.y=0;
			vel.angular.z=0;
			//ROS_INFO("4");
			vel_pub.publish(vel);
   	 	}
	}

   
	
	
}
else if(order == 83)
{
	vel.linear.x=0;
	vel.linear.y=0;
	vel.angular.z=0;
	//ROS_INFO("Stop");
	vel_pub.publish(vel);
}
else if(order==103)
{
    vel.linear.x=0.25;
    vel.linear.y=0;
    vel.angular.z=0;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==115)
{
    vel.linear.x=0;
    vel.linear.y=0;
    vel.angular.z=0;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==114)
{
    vel.linear.x=0.2;
    vel.linear.y=0;
    vel.angular.z=-0.2;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==108)
{
    vel.linear.x=0.2;
    vel.linear.y=0;
    vel.angular.z=0.2;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==97)
{
    vel.linear.x=0;
    vel.linear.y=0;
    vel.angular.z=0.2;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==100)
{
    vel.linear.x=0;
    vel.linear.y=0;
    vel.angular.z=-0.2;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==69)
{
    
	vel.linear.x=-0.2;
    	vel.linear.y=0;
    	vel.angular.z=0;
    	vel_pub.publish(vel);
	ros::Duration(6).sleep();

order=999;

	vel.linear.x=0;
    	vel.linear.y=0;
    	vel.angular.z=0;
    	vel_pub.publish(vel);
order=999;
	ros::Duration(1).sleep();
	
}


	front=0;
	ros::spinOnce();
	rate.sleep();
}	
    ROS_INFO("Eak113");
  return 0;
}

