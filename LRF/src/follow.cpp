#include <ros/ros.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <math.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>

#define PI 3.14159265
double x;
double y;
int left=0;
int right=0;
int front=0;
int order=0;
float ok;
int i;
float laserx[501];
float lasery[501];
float obstaclex[500];
float obstacley[500];
int human=0;
int lift=0;
int ex=0;

void Callback(const std_msgs::Float64::ConstPtr& msg)
{ 
    x=msg->data;
}

void Callback2(const std_msgs::Float64::ConstPtr& mss)
{ 
    y=mss->data;
}


void Callback4(const std_msgs::Float64::ConstPtr& msa)
{ 
    ok=msa->data;
}

void Callback5(const std_msgs::Int64::ConstPtr& msf)
{ 
    order=msf->data;
}

void Callback3(const sensor_msgs::LaserScanConstPtr& msv)
{
/* 
for(i=215;i<285;i++)
	{
    obstaclex[i] = msv->ranges[i]*cos(i*0.36*PI/180);
    obstacley[i] = msv->ranges[i]*sin(i*0.36*PI/180);
	if(obstacley[i]< 0.60 && obstacley[i] > 0.1)
		{
		front=1;
		//ROS_INFO("obstacle",obstacley[i]);
		}
	}
for(i=50;i<214;i++)
	{
	if(msv->ranges[i] < 0.2 && msv->ranges[i] > 0.1)
		{
		left=1;
		}
	}
for(i=286;i<500;i++)
	{
	if(msv->ranges[i] < 0.2 && msv->ranges[i] > 0.1)
		{
		right=1;
		}
	}
*/
}

int main(int argc, char** argv)
{   
    ros::init(argc, argv, "Follow");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("positionx",100, &Callback); 
    ros::Subscriber sub2 = n.subscribe("positiony",100, &Callback2); 
    ros::Subscriber sub4 = n.subscribe("tracking",100, &Callback4); 
    ros::Subscriber sub3 = n.subscribe("scan1",100, &Callback3); 
    ros::Subscriber sub5 = n.subscribe("order",100, &Callback5); 
    ros::Publisher chatter_pub  = n.advertise<geometry_msgs::Twist>("cmd_vel",100);
    ros::Publisher chatter_pub2  = n.advertise<std_msgs::Int64>("order",100);
    ros::Publisher chatter_pub5  = n.advertise<std_msgs::Float64>("joint1/command",1000);  
    ros::Publisher chatter_pub6  = n.advertise<std_msgs::Float64>("joint2/command",1000); 
    geometry_msgs::Twist vel;
    std_msgs::Int64 a;
    std_msgs::Float64 b;
    std_msgs::Float64 c;
    ros::Rate rate(10);
    while(ros::ok())
{ 
  
if(order==70)
{
    if(front==0 )
    {
	if(ok>0)
	{
	if(y>=0.8&&y<1.4)
		{
		if(x>0.105)
			{
			vel.linear.x=0.32;
			vel.linear.y=0;
			vel.angular.z=-0.32; //-0.3
			//ROS_INFO("Right");
			}
		else if(x<-0.105)
			{
			vel.linear.x=0.32;
			vel.linear.y=0;
			vel.angular.z=0.32; //0.3
			//ROS_INFO("Left");
			}
		else if(x<=0.105&&x>=-0.105)
			{
			vel.linear.x=0.4;
			vel.linear.y=0;
			vel.angular.z=0;
			//ROS_INFO("front");
			}
		}	

	else if(y>0.7&&y<0.8)
		{
		if(x>0.1)
			{
			vel.linear.x=0.25; //0.16
			vel.linear.y=0;
			vel.angular.z=-0.27;  //-0.2
			//ROS_INFO("Right");
			}
		else if(x<-0.1)
			{
			vel.linear.x=0.25; //0.16
			vel.linear.y=0;
			vel.angular.z=0.27; //0.2
			//ROS_INFO("Left");
			}
		else if(x<=0.1&&x>=-0.1)
			{
			vel.linear.x=0.27;  //0.2
			vel.linear.y=0;
			vel.angular.z=0;
			//ROS_INFO("front");
			}
		}

	else if(y>0.6 && y<0.7)
		{
			vel.linear.x=0;
			vel.linear.y=0;
			vel.angular.z=0;
			//ROS_INFO("stop");
			
		}
	else if(y<=0.60 && y>0.1)
		{
		vel.linear.x=0;
		vel.linear.y=0;
		vel.angular.z=0;
		//ROS_INFO("back");
		}
	
	}
	else
	{
	vel.linear.x=0;
	vel.linear.y=0;
	vel.angular.z=0;
	//ROS_INFO("STOP");
	}
    }

}
else if(order == 83)
{
	vel.linear.x=0;
	vel.linear.y=0;
	vel.angular.z=0;
	//ROS_INFO("Stop");
	vel_pub.publish(vel);
}
else if(order==103)
{
    vel.linear.x=0.25;
    vel.linear.y=0;
    vel.angular.z=0;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==115)
{
    vel.linear.x=0;
    vel.linear.y=0;
    vel.angular.z=0;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==114)
{
    vel.linear.x=0.2;
    vel.linear.y=0;
    vel.angular.z=-0.2;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==108)
{
    vel.linear.x=0.2;
    vel.linear.y=0;
    vel.angular.z=0.2;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==97)
{
    vel.linear.x=0;
    vel.linear.y=0;
    vel.angular.z=0.2;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}
else if(order==100)
{
    vel.linear.x=0;
    vel.linear.y=0;
    vel.angular.z=-0.2;
    //ROS_INFO("Eak");
    vel_pub.publish(vel);
}

    left=0;
    front=0;
    right=0;
    chatter_pub.publish(vel);
    ros::spinOnce();
    rate.sleep();
}	
  return 0;
}

