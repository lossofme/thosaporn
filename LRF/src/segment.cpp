// update 20-1-13

#include <ros/ros.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <math.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>

#define PI 3.14159265
float obs[500];
float leg[500];
float x[500];
float y[500];
float D[500];
float P[500];
float theta[500];
int i=0;
int j=0;
int data=0;
std_msgs::Header temp_header;
float temp_range_min; 
float temp_range_max;
float temp_angle_min;
float temp_angle_increment;
float temp_time_increment;
float temp_scan_time; 
float temp_ranges[500];
int state=1;


void Callback(const sensor_msgs::LaserScanConstPtr& msg)
{ 

  temp_header          = msg->header;
  temp_range_min       = msg->range_min;
  temp_range_max       = msg->range_max;
  temp_angle_min       = msg->angle_min;
  temp_angle_increment = msg->angle_increment;
  temp_time_increment  = msg->time_increment;
  temp_scan_time       = msg->scan_time;
  
	for(i=0;i<=500;i++)
	{
		obs[i] = msg->ranges[i];
		leg[i] = msg->ranges[i];
		if(msg->ranges[i]<5.0)
		{
		x[i] = msg->ranges[i]*cos(i*0.36*PI/180);
		y[i] = msg->ranges[i]*sin(i*0.36*PI/180);
		}
		else
		{
		x[i]=0;
		y[i]=0;
		}
		
	}
	for(i=0;i<=100;i++)
	{	
		D[5*i] = sqrt( pow(x[5*i]-x[(5*i)+5],2) + pow(y[5*i]-y[(5*i)+5],2));
		D[(5*i)+1] = sqrt( pow(x[(5*i)+1]-x[(5*i)+6],2) + pow(y[(5*i)+1]-y[(5*i)+6],2));
		D[(5*i)+2] = sqrt( pow(x[(5*i)+2]-x[(5*i)+7],2) + pow(y[(5*i)+2]-y[(5*i)+7],2));
		D[(5*i)+3] = sqrt( pow(x[(5*i)+3]-x[(5*i)+8],2) + pow(y[(5*i)+3]-y[(5*i)+8],2));
		D[(5*i)+4] = sqrt( pow(x[(5*i)+4]-x[(5*i)+9],2) + pow(y[(5*i)+4]-y[(5*i)+9],2));
	}
	for(i=0;i<=100;i++)
	{	
		P[(5*i)] = sqrt( pow(x[5*i]-x[(5*i)+10],2) + pow(y[5*i]-y[(5*i)+10],2));
		P[(5*i)+1] = sqrt( pow(x[(5*i)+1]-x[(5*i)+11],2) + pow(y[(5*i)+11]-y[(5*i)+10],2));
		P[(5*i)+2] = sqrt( pow(x[(5*i)+1]-x[(5*i)+12],2) + pow(y[(5*i)+11]-y[(5*i)+10],2));
		P[(5*i)+3] = sqrt( pow(x[(5*i)+1]-x[(5*i)+13],2) + pow(y[(5*i)+11]-y[(5*i)+10],2));
		P[(5*i)+4] = sqrt( pow(x[(5*i)+1]-x[(5*i)+14],2) + pow(y[(5*i)+11]-y[(5*i)+10],2));
	}
	for(i=0;i<=90;i++)
	{	
		theta[(5*i)] = acos( ((P[(5*i)]*P[(5*i)]*-1) + (D[(5*i)]*D[(5*i)]) + (D[(5*i)+5]*D[(5*i)+5]) )/ (2 * D[(5*i)] * D[(5*i)+5] ) )*180/PI;	
		theta[(5*i)+1] = acos( ((P[(5*i)+1]*P[(5*i)+1]*-1) + (D[(5*i)+1]*D[(5*i)+1]) + (D[(5*i)+6]*D[(5*i)+6]) )/ (2 * D[(5*i)+1] * D[(5*i)+6] ) )*180/PI;
		theta[(5*i)+2] = acos( ((P[(5*i)+2]*P[(5*i)+2]*-1) + (D[(5*i)+2]*D[(5*i)+2]) + (D[(5*i)+7]*D[(5*i)+7]) )/ (2 * D[(5*i)+2] * D[(5*i)+7] ) )*180/PI;
		theta[(5*i)+3] = acos( ((P[(5*i)+3]*P[(5*i)+3]*-1) + (D[(5*i)+3]*D[(5*i)+3]) + (D[(5*i)+8]*D[(5*i)+8]) )/ (2 * D[(5*i)+3] * D[(5*i)+8] ) )*180/PI;
		theta[(5*i)+4] = acos( ((P[(5*i)+4]*P[(5*i)+4]*-1) + (D[(5*i)+4]*D[(5*i)+4]) + (D[(5*i)+9]*D[(5*i)+9]) )/ (2 * D[(5*i)+4] * D[(5*i)+9] ) )*180/PI;
	}
	for(i=0;i<=500;i++)
	{	
		if(theta[i]<160 && theta[i]>100){temp_ranges[i] = msg->ranges[i]; }//160 - 100
		else{temp_ranges[i] =0;}	
		if(i==0){temp_ranges[i] =0;}
	}
	//ROS_INFO("%f %f %f %f %f",theta[5],theta[10],theta[20],theta[25],theta[35]);
	ROS_INFO("-------");



}

int main(int argc, char** argv)
{   
    ros::init(argc, argv, "Segment");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("scan",100, &Callback); 
    ros::Publisher pub = n.advertise<sensor_msgs::LaserScan>("segment",100); 
    ros::Rate rate(10);
    while(ros::ok())
{    
  sensor_msgs::LaserScan::Ptr segment;
  segment = boost::make_shared<sensor_msgs::LaserScan>();
  segment->header          = temp_header;
  segment->range_min       = temp_range_min;
  segment->range_max       = temp_range_max;
  segment->angle_min       = temp_angle_min;
  segment->angle_increment = temp_angle_increment;
  segment->time_increment  = temp_time_increment;
  segment->scan_time       = temp_scan_time;
  unsigned int size_sparse = 500;
  segment->ranges.resize(size_sparse);
	for(i=0;i<=500;i++)
	{
	if(i<0 || i>500){segment->ranges[i]=0;}	
	else
	{
	if(temp_ranges[i] < 5.0)
		{
		segment->ranges[i]=temp_ranges[i] ; 
		}
	else
		{
		segment->ranges[i]=0;
		}
	}
        if(segment->ranges[i]>0.1){ROS_INFO("%d",i);}

	}

    pub.publish(segment);
    ros::spinOnce();
    rate.sleep();
}	
  return 0;

}

