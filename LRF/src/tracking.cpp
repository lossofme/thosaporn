#include <ros/ros.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <math.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>

#define PI 3.14159265
double xold=0;
double yold=0.8;
double xnew;
double ynew;
double x;
double y;
int order;

void Callback(const std_msgs::Float64::ConstPtr& msg)
{ 
    xnew=msg->data;
}
void Callback2(const std_msgs::Float64::ConstPtr& mss)
{ 
    ynew=mss->data;
}
void Callback3(const std_msgs::Int64::ConstPtr& msd)
{ 
    order=msd->data;
}

int main(int argc, char** argv)
{   
    ros::init(argc, argv, "Follow");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("positionx",1000, &Callback); 
    ros::Subscriber sub2 = n.subscribe("positiony",1000, &Callback2); 
    ros::Subscriber sub3 = n.subscribe("order",1000, &Callback3);
    ros::Publisher chatter_pub  = n.advertise<geometry_msgs::Twist>("cmd_vel",1000);
    geometry_msgs::Twist vel;
    ros::Rate rate(100);
    while(ros::ok())
{   
    if(order=115)
	{
	if(xnew>(xold-0.15)&&xnew<(xold+0.15))
		{
		x=xnew;
		xold=xnew;
		}
	else
		{
		x=xold;
		xold=xold;
		}
	if(ynew>(yold-0.2)&&ynew<(yold+0.2))
		{
		y=ynew;
		yold=ynew;
		}
	else
		{
		y=yold;
		yold=yold;
		}
	}
 
    ros::spinOnce();
    rate.sleep();
}	
  return 0;
}

