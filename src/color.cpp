//#include "stdafx.h"
#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <pcl/io/pcd_io.h>
#include <sensor_msgs/Image.h>

#include <stdlib.h>
#include "opencv2/core/core_c.h"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <stdio.h>
#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"


#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

//------------------------------------------------------------------------------------------------------------------------------------------//

using namespace std; 
using namespace cv; 

int lowerH=0;
int lowerS=0;
int lowerV=0;

int upperH=180;
int upperS=256;
int upperV=256;

//This function threshold the HSV image and create a binary image
IplImage* GetThresholdedImage(IplImage* imgHSV){

IplImage* imgThresh=cvCreateImage(cvGetSize(imgHSV),IPL_DEPTH_8U, 1);
cvInRangeS(imgHSV, cvScalar(lowerH,lowerS,lowerV), cvScalar(upperH,upperS,upperV), imgThresh);

return imgThresh;

}

//This function create two windows and 6 trackbars for the "Ball" window
void setwindowSettings(){
	cvNamedWindow("Video");
	cvNamedWindow("Tuning");
//trackber H
	cvCreateTrackbar("LowerH", "Tuning", &lowerH, 180, NULL);
        cvCreateTrackbar("UpperH", "Tuning", &upperH, 180, NULL);
//trackber S
	cvCreateTrackbar("LowerS", "Tuning", &lowerS, 256, NULL);
        cvCreateTrackbar("UpperS", "Tuning", &upperS, 256, NULL);
//trackber V
	cvCreateTrackbar("LowerV", "Tuning", &lowerV, 256, NULL);
        cvCreateTrackbar("UpperV", "Tuning", &upperV, 256, NULL);
}



//-------------------------------------------------------------------------------------------------------------------------------------------//

namespace enc = sensor_msgs::image_encodings;

static const char WINDOW[] = "Image window";

class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  cv_bridge::CvImagePtr cv_ptr;
  
public:
  ImageConverter()
    : it_(nh_)
  {
    image_pub_ = it_.advertise("out_camera", 1);
    image_sub_ = it_.subscribe("/camera/rgb/image_rect_color", 1, &ImageConverter::imageCb, this);
  }


  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, enc::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
	

    //if (cv_ptr->image.rows > 60 && cv_ptr->image.cols > 60)
    //    cv::circle(cv_ptr->image, cv::Point(320, 240), 10, CV_RGB(255,0,0));

    //    cv::imshow(WINDOW, cv_ptr->image);
    //    cv::waitKey(3);
    
    image_pub_.publish(cv_ptr->toImageMsg());
  }
};

int cvcapture()
{

	CvCapture* capture =0;
	capture = cvCaptureFromCAM(0);

        IplImage* frame=0;
	setwindowSettings();

	//iterate through each frames of the video
		while(true)
		{
			frame = cvQueryFrame(capture);
			if(!frame)  break;
			
		frame=cvCloneImage(frame);
		cv::Mat mat_frame(frame);
		IplImage* imgHSV = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 3);
		cv::Mat mat_imgHSV(imgHSV);

		cvtColor(mat_frame, mat_imgHSV, CV_BGR2HSV ); //Change the color format from BGR to HSV

		IplImage* imgThresh = GetThresholdedImage(imgHSV);
		cvShowImage("Tuning", imgThresh);
                cvShowImage("Video", frame);
	//Clean up used images
		cvReleaseImage(&imgHSV);
		cvReleaseImage(&imgThresh);
		cvReleaseImage(&frame);
	//Wait 80mS
		int c = cvWaitKey(80);
	//If 'ESC' is pressed, break the loop
		if((char)c==27 ) break;
		}

	cvDestroyAllWindows();
	cvReleaseCapture(&capture);

       return 0;
}


//------------------------------------------------------------------------------------------------------------------------------------------//



int main(int argc, char** argv){

  ros::init(argc, argv, "sub_Color");
  ros::NodeHandle nh;
	cvcapture();
	//ImageConverter ic;
  ros::spin();
return 0;
}
