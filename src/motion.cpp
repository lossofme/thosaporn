#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <pcl/io/pcd_io.h>
#include <sensor_msgs/Image.h>

#include <stdlib.h>
#include "opencv2/core/core_c.h"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <stdio.h>
#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"


#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <iostream>
#include <opencv2/opencv.hpp>

#include "cv.h"
#include "highgui.h"
#include "stdlib.h"
#include <std_msgs/Int64.h>

//------------------------------------------------------------------------------------------------------------------------------------------//

using namespace std; 
using namespace cv; 

std_msgs::Int64 value;


//------------------------------------------------------------------------------------------------------------------------------------------//



int main(int argc, char** argv){

  ros::init(argc, argv, "motion");
  ros::NodeHandle nh;
value.data = 2;
  ros::Publisher chatter_pub = nh.advertise<std_msgs::Int64>("command",100);
  ros::Rate rate(100); 
	while(ros::ok()) 
	{ 
		  
	cvNamedWindow("My Window", CV_WINDOW_AUTOSIZE);
    CvCapture *input;
   
    input = cvCaptureFromCAM(0);

    CvSize imgSize;
    IplImage* frame = cvQueryFrame(input);
    imgSize = cvGetSize(frame);

    IplImage* greyImage = cvCreateImage( imgSize, IPL_DEPTH_8U, 1);
    IplImage* colourImage;
    IplImage* movingAverage = cvCreateImage( imgSize, IPL_DEPTH_32F, 3);
    IplImage* difference;
    IplImage* temp;
    IplImage* motionHistory = cvCreateImage( imgSize, IPL_DEPTH_8U, 3);

    CvRect bndRect = cvRect(0,0,0,0);

    CvPoint pt1, pt2;

    CvFont font;


    char* outFilename = "E:\\outputMovie.avi";
    CvVideoWriter* outputMovie = cvCreateVideoWriter(outFilename,CV_FOURCC('F', 'L', 'V', 'I'), 29.97, cvSize(720, 576));

    int prevX = 0;
    int numPeople = 0;

    char wow[65];

    int avgX = 0;

    bool first = true;

    int closestToLeft = 0;
    int closestToRight = 640;

    for(;;)
        {
        colourImage = cvQueryFrame(input);

        if( !colourImage )
            {
            break;
            }
        if(first)
            {
            difference = cvCloneImage(colourImage);
            temp = cvCloneImage(colourImage);
            cvConvertScale(colourImage, movingAverage, 1.0, 0.0);
            first = false;
            }
        else

            {
            cvRunningAvg(colourImage, movingAverage, 0.020, NULL);
            }

        cvConvertScale(movingAverage,temp, 1.0, 0.0);

        cvAbsDiff(colourImage,temp,difference);

        cvCvtColor(difference,greyImage,CV_RGB2GRAY);

        cvThreshold(greyImage, greyImage, 70, 255, CV_THRESH_BINARY);

        cvDilate(greyImage, greyImage, 0, 18);
        cvErode(greyImage, greyImage, 0, 10);

        CvMemStorage* storage = cvCreateMemStorage(0);
        CvSeq* contour = 0;

        cvFindContours( greyImage, storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );


        for( ; contour != 0; contour = contour->h_next )
        {
            bndRect = cvBoundingRect(contour, 0);
            pt1.x = bndRect.x;
            pt1.y = bndRect.y;
            pt2.x = bndRect.x + bndRect.width;
            pt2.y = bndRect.y + bndRect.height;

            avgX = (pt1.x + pt2.x) / 2;

		value.data=0;
		chatter_pub.publish(value); 

        if(avgX > 90 && avgX < 550)
        {
            if(closestToLeft >= 88 && closestToLeft <= 90)
                {
                if(avgX > prevX)
                    {
                    numPeople++;
                    closestToLeft = 0;
                    }

                }
            else if(closestToRight >= 550 && closestToRight <= 554)
                {
                if(avgX < prevX)
                    {
                    numPeople++;
                    closestToRight = 640;
                    }

                }
            cvRectangle(colourImage, pt1, pt2, CV_RGB(255,0,0), 1);

		if(avgX > 90 && avgX < 245)
        		{	
				value.data=2;
				chatter_pub.publish(value);
 			}
		if(avgX > 244 && avgX < 397)
        		{	
				value.data=1;
				chatter_pub.publish(value);
 			}
		if(avgX > 396 && avgX < 550)
        		{	
				value.data=3;
				chatter_pub.publish(value);
 			}
        }
	

        if(avgX > closestToLeft && avgX <= 90)
            {
            closestToLeft = avgX;
            }

        if(avgX < closestToRight && avgX >= 550)
            {
            closestToRight = avgX;
            }

        prevX = avgX;
        prevX = avgX;

        }

        cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 0.8, 0.8, 0, 2);
        cvShowImage("My Window", colourImage);

        cvWaitKey(10);
        cvWriteFrame(outputMovie, colourImage);

    }


    cvReleaseImage(&temp);
    cvReleaseImage(&difference);
    cvReleaseImage(&greyImage);
    cvReleaseImage(&movingAverage);
    cvDestroyWindow("My Window");

    cvReleaseCapture(&input);
    cvReleaseVideoWriter(&outputMovie);
	} 
	return 0;
}
