#include <ros/ros.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <sstream> 
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Int64.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <move_base_msgs/MoveBaseActionFeedback.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

int data[5];


void Callback(const sensor_msgs::Joy& msg)
{ 
	data[0] = msg.buttons[0];
	data[1] = msg.buttons[1];
	data[2] = msg.buttons[2];
	data[3] = msg.buttons[3];
}



int main(int argc, char** argv)
{   
    ros::init(argc, argv, "novoice");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("joy",1000, &Callback);
    ros::Publisher chatter_pub  = n.advertise<geometry_msgs::PoseStamped>("move_base_simple/goal",1000); 
    ros::Rate rate(5);
    geometry_msgs::PoseStamped goal;
  while(ros::ok()){
//---table A
   if(data[1]==1)  
   {
   goal.header.frame_id="/map"; 
   goal.pose.position.x=2.1; 
   goal.pose.position.y=1.15; 
   goal.pose.orientation.z=0.7; 
   goal.pose.orientation.w=0.7; 
   chatter_pub.publish(goal); 
   }
//---table B
 else if(data[2]==1)
  {
  goal.header.frame_id="/map"; 
  goal.pose.position.x=4.58; 
  goal.pose.position.y=1.5; 
  goal.pose.orientation.z=0.47; 
  goal.pose.orientation.w=0.88; 
  chatter_pub.publish(goal);
  }
//---table C
 else if(data[3]==1)
  {
  goal.header.frame_id="/map"; 
  goal.pose.position.x=3.28; 
  goal.pose.position.y=0.2; 
  goal.pose.orientation.z=-0.7; 
  goal.pose.orientation.w=0.7; 
  chatter_pub.publish(goal);
  }
//---start
 else if(data[0]==1)
  {
  goal.header.frame_id="/map"; 
  goal.pose.position.x=0; 
  goal.pose.position.y=0; 
  goal.pose.orientation.z=0; 
  goal.pose.orientation.w=1; 
  chatter_pub.publish(goal);
  }
  ros::spinOnce();
  rate.sleep();
}	
  return 0;
}


