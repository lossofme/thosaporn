#include "ros/ros.h"
#include "std_msgs/Int64.h"
int order;

void chatterCallback(const std_msgs::Int64& msg)
{
  if(msg.data!=10)
	order=msg.data;
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("order", 1000, chatterCallback);

 ros::Rate rate(5);
  while(ros::ok())
  {
  ROS_INFO("%d \n",order);
  order=0;
  ros::spinOnce();
  rate.sleep();
  }	

  return 0;
}
