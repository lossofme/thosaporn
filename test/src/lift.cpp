#include <ros/ros.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <math.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>

#define PI 3.14159265
int start=1;

int main(int argc, char** argv)
{   
    ros::init(argc, argv, "Follow");
    ros::NodeHandle n;
    ros::Publisher chatter_pub  = n.advertise<geometry_msgs::Twist>("cmd_vel",100);
    geometry_msgs::Twist vel;
    ros::Rate rate(10);
    while(ros::ok())
{ 
	if(start==1)
{
	vel.linear.x=0;
	vel.linear.y=0;
	vel.angular.z=0;
	chatter_pub.publish(vel);
	ros::Duration(0.5).sleep();

	ROS_INFO("human lose");
	vel.linear.x=0.1;
	vel.linear.y=0;
	vel.angular.z=0;
	chatter_pub.publish(vel);
	ros::Duration(1).sleep();

	ROS_INFO("human lose2");
	vel.linear.x=0;
	vel.linear.y=0;
	vel.angular.z=0;
	chatter_pub.publish(vel);
	ros::Duration(0.5).sleep();

	start=2;
}

    ros::spinOnce();
    rate.sleep();
}	
  return 0;
}

