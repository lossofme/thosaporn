/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <ros/ros.h>
#include <std_msgs/Int64.h>
#include <std_msgs/String.h>
void error(const char *msg)
{
    perror(msg);
   // exit(1);
}

int main(int argc, char** argv)
{
   ros::init(argc, argv, "center");//ตั้งตาทที่ตั้งไว้ที่ cmake
    ros::NodeHandle nh;//ประกาศโนด
    ros::Publisher chatter_pub  = nh.advertise<std_msgs::Int64>("order",1000);  
    ros::Rate rate(1000);//กำหนดค่าในการหลับ มีพื่อเครื่องทำงานไม่หนั
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     std_msgs::Int64 a;
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi("51717");
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
   while(ros::ok()){

     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
     if (newsockfd < 0) 
          error("ERROR on accept");
     
     bzero(buffer,256);
     n = read(newsockfd,buffer,255);


     printf("Here is the message: %s\n",buffer);
      a.data= buffer[0];
      chatter_pub.publish(a);
     n = write(newsockfd,"I got your message",18);
     if (n < 0) error("ERROR writing to socket");
     //close(newsockfd);
     //close(sockfd);
   //      ros::spinOnce();
   //rate.sleep(); 
}
  return 0;
}
