#include<stdio.h>
#include<ros/ros.h>
#include<sensor_msgs/Joy.h>
#include<geometry_msgs/Twist.h>

float x=0;
float z=0;
geometry_msgs::Twist vel;
geometry_msgs::Twist velo;
void Callback(const sensor_msgs::Joy& msg) 
{

if(msg.axes[1]==1.0)
{
x=0.08;
ROS_INFO("%.1f",x);
}
if(msg.axes[1]==-1.0)
{
x=-0.08;
ROS_INFO("%.1f",x);
}
if(msg.axes[1]==0.0)
x=0.0;



if(msg.axes[0]==1.0)
{
z=0.3;
ROS_INFO("%.1f",z);
}
if(msg.axes[0]==-1.0)
{
z=-0.3;
ROS_INFO("%.1f",z);
}
if(msg.axes[0]==0.0)
z=0.0;


}
int main(int argc, char** argv)
{
	ros::init(argc, argv, "test");
	ros::NodeHandle n; 
	ros::Subscriber sub = n.subscribe("joy",100, &Callback); 
	ros::Publisher chatter_pub = n.advertise<geometry_msgs::Twist>("cmd_vel",100); 
	ros::Publisher chatter_pub2 = n.advertise<geometry_msgs::Twist>("velocity",100);
	ros::Publisher chatter_pub3 = n.advertise<geometry_msgs::Twist>("turtlebot",100);
	ros::Rate rate(100); 
while(ros::ok()) 
{  
	vel.linear.x=x;
	vel.angular.z=z;
	chatter_pub.publish( vel ); 
	chatter_pub2.publish( vel );
	chatter_pub3.publish( vel );
	ros::spinOnce(); 
	rate.sleep();
}
return 0;
}
