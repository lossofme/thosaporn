#include <ros/ros.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <sstream> 
#include <sensor_msgs/Joy.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int64.h>
#include <string.h>


int data[6];
int order;

void Callback(const sensor_msgs::Joy& msg)
{ 
	data[0] = msg.buttons[0];
	data[1] = msg.buttons[1];
	data[2] = msg.buttons[2];
	data[3] = msg.buttons[3];
	data[4] = msg.buttons[4];
	data[5] = msg.buttons[5];
}
void Callback2(const std_msgs::Int64& msg)
{
  if(msg.data!=10)
	order=msg.data;
}

int main(int argc, char** argv)
{   
    ros::init(argc, argv, "control_arm");//ตั้งตาทที่ตั้งไว้ที่ cmake
    ros::NodeHandle n;//ประกาศโนด
    ros::Subscriber sub = n.subscribe("joy",1000, &Callback);
    ros::Subscriber sub2 = n.subscribe("order",1000, &Callback2);
    ros::Publisher chatter_pub1  = n.advertise<std_msgs::Float64>("arm1motor1/command",1000);  
    ros::Publisher chatter_pub2  = n.advertise<std_msgs::Float64>("arm1motor2/command",1000);  
    ros::Publisher chatter_pub3  = n.advertise<std_msgs::Float64>("arm1motor3/command",1000);  
    ros::Publisher chatter_pub4  = n.advertise<std_msgs::Float64>("arm1motor4/command",1000);  
    ros::Publisher chatter_pub5  = n.advertise<std_msgs::Float64>("arm1motor5/command",1000); 
    ros::Publisher chatter_pub6  = n.advertise<std_msgs::Float64>("arm2motor1/command",1000);  
    ros::Publisher chatter_pub7  = n.advertise<std_msgs::Float64>("arm2motor2/command",1000);  
    ros::Publisher chatter_pub8  = n.advertise<std_msgs::Float64>("arm2motor3/command",1000);  
    ros::Publisher chatter_pub9  = n.advertise<std_msgs::Float64>("arm2motor4/command",1000);  
    ros::Publisher chatter_pub10  = n.advertise<std_msgs::Float64>("arm2motor5/command",1000); 
    ros::Publisher chatter_pub11  = n.advertise<std_msgs::Float64>("joint1/command",1000);  
    ros::Publisher chatter_pub12  = n.advertise<std_msgs::Float64>("joint2/command",1000);    
    ros::Rate rate(5);
    std_msgs::Float64 arm1motor1;
    std_msgs::Float64 arm1motor2;
    std_msgs::Float64 arm1motor3;
    std_msgs::Float64 arm1motor4;
    std_msgs::Float64 arm1motor5;
    std_msgs::Float64 arm2motor1;
    std_msgs::Float64 arm2motor2;
    std_msgs::Float64 arm2motor3;
    std_msgs::Float64 arm2motor4;
    std_msgs::Float64 arm2motor5;
    std_msgs::Float64 neck1;
    std_msgs::Float64 neck2;

  while(ros::ok())
  {
	if(data[1]==1)
	{
		arm1motor1.data=0;
		arm1motor2.data=1;
		arm1motor3.data=1.9;
		arm1motor4.data=-0.8;
		arm1motor5.data=-0.3;
		arm2motor1.data=0;
		arm2motor2.data=1;
		arm2motor3.data=1.9;
		arm2motor4.data=-0.8;
		arm2motor5.data=-0.4;

  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		chatter_pub5.publish(arm1motor5);
  		chatter_pub6.publish(arm2motor1);
  		chatter_pub7.publish(arm2motor2);
 		chatter_pub8.publish(arm2motor3);
 		chatter_pub9.publish(arm2motor4);
  		chatter_pub10.publish(arm2motor5);
		order = 0;
	}
	if(data[0]==1)
	{
		arm1motor1.data=0;
		arm1motor2.data=0;
		arm1motor3.data=0;
		arm1motor4.data=0;
		arm2motor1.data=0;
		arm2motor2.data=0;
		arm2motor3.data=0;
		arm2motor4.data=0;
		neck1.data=0;
		neck2.data=0;

  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		//chatter_pub5.publish(arm1motor5);
  		chatter_pub6.publish(arm2motor1);
  		chatter_pub7.publish(arm2motor2);
 		chatter_pub8.publish(arm2motor3);
 		chatter_pub9.publish(arm2motor4);
  		//chatter_pub10.publish(arm2motor5);
 		chatter_pub11.publish(neck1);
 		chatter_pub12.publish(neck2);
		order = 0;
	}
	
	if(data[2]==1 )
	{
		arm1motor1.data=-1.2;
		arm1motor2.data=0.4;
		arm1motor3.data=1.5;
		arm1motor4.data=-1.7;
		arm2motor1.data=1;
		arm2motor2.data=0.4;
		arm2motor3.data=1.5;
		arm2motor4.data=-1.7;
		neck1.data=0;
		neck2.data=0;
  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		//chatter_pub5.publish(arm1motor5);
  		chatter_pub6.publish(arm2motor1);
  		chatter_pub7.publish(arm2motor2);
 		chatter_pub8.publish(arm2motor3);
 		chatter_pub9.publish(arm2motor4);
  		//chatter_pub10.publish(arm2motor5);
 		chatter_pub11.publish(neck1);
 		chatter_pub12.publish(neck2);
		ros::Duration(5).sleep();
		neck1.data=0;
		neck2.data=0.7;
 		chatter_pub11.publish(neck1);
 		chatter_pub12.publish(neck2);
		ros::Duration(1.5).sleep();
		neck1.data=0;
		neck2.data=0.0;
 		chatter_pub11.publish(neck1);
 		chatter_pub12.publish(neck2);
		ros::Duration(1.5).sleep();
		arm1motor1.data=0;
		arm1motor2.data=1;
		arm1motor3.data=1.9;
		arm1motor4.data=-0.8;
		arm1motor5.data=-0.3;
		arm2motor1.data=0;
		arm2motor2.data=1;
		arm2motor3.data=1.9;
		arm2motor4.data=-0.8;
		arm2motor5.data=-0.4;

  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		chatter_pub5.publish(arm1motor5);
  		chatter_pub6.publish(arm2motor1);
  		chatter_pub7.publish(arm2motor2);
 		chatter_pub8.publish(arm2motor3);
 		chatter_pub9.publish(arm2motor4);
  		chatter_pub10.publish(arm2motor5);
	}
	if(data[3]==1 )
	{
		arm1motor1.data=0.5;
		arm1motor2.data=-1.4;
		arm1motor3.data=1.7;
		arm1motor4.data=-0.2;
		arm1motor5.data=-0.3;
		
  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		chatter_pub5.publish(arm1motor5);
		ros::Duration(6).sleep();
		arm1motor1.data=-0.5;
		arm1motor2.data=-1.4;
		arm1motor3.data=1.7;
		arm1motor4.data=-0.2;
		arm1motor5.data=-0.3;
		
  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		chatter_pub5.publish(arm1motor5);
		ros::Duration(1.5).sleep();
		arm1motor1.data=0.5;
		arm1motor2.data=-1.4;
		arm1motor3.data=1.7;
		arm1motor4.data=-0.2;
		arm1motor5.data=-0.3;
		
  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		chatter_pub5.publish(arm1motor5);
		ros::Duration(1.5).sleep();
		arm1motor1.data=-0.5;
		arm1motor2.data=-1.4;
		arm1motor3.data=1.7;
		arm1motor4.data=-0.2;
		arm1motor5.data=-0.3;
		
  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		chatter_pub5.publish(arm1motor5);
		ros::Duration(1.5).sleep();
		arm1motor1.data=0.5;
		arm1motor2.data=-1.4;
		arm1motor3.data=1.7;
		arm1motor4.data=-0.2;
		arm1motor5.data=-0.3;
		
  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		chatter_pub5.publish(arm1motor5);
		ros::Duration(1.5).sleep();
		arm1motor1.data=-0.5;
		arm1motor2.data=-1.4;
		arm1motor3.data=1.7;
		arm1motor4.data=-0.2;
		arm1motor5.data=-0.3;
		
  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		chatter_pub5.publish(arm1motor5);
		ros::Duration(1.5).sleep();
		arm1motor1.data=0.5;
		arm1motor2.data=-1.4;
		arm1motor3.data=1.7;
		arm1motor4.data=-0.2;
		arm1motor5.data=-0.3;
		
  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		chatter_pub5.publish(arm1motor5);
		ros::Duration(1.5).sleep();
		arm1motor1.data=0;
		arm1motor2.data=1;
		arm1motor3.data=1.9;
		arm1motor4.data=-0.8;
		arm1motor5.data=-0.3;
		arm2motor1.data=0;
		arm2motor2.data=1;
		arm2motor3.data=1.9;
		arm2motor4.data=-0.8;
		arm2motor5.data=-0.4;

  		chatter_pub1.publish(arm1motor1);
  		chatter_pub2.publish(arm1motor2);
  		chatter_pub3.publish(arm1motor3);
  		chatter_pub4.publish(arm1motor4);
  		chatter_pub5.publish(arm1motor5);
  		chatter_pub6.publish(arm2motor1);
  		chatter_pub7.publish(arm2motor2);
 		chatter_pub8.publish(arm2motor3);
 		chatter_pub9.publish(arm2motor4);
  		chatter_pub10.publish(arm2motor5);
		order = 0;
	}
	if(data[4]==1 )
	{
		
	}
  ros::spinOnce();
  rate.sleep();
  }	
  return 0;
}


