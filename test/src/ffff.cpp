#include <ros/ros.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <sstream>  //ใช้ทุกครั้ง
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <sensor_msgs/Joy.h>	//include sensor  อ้างอิงไปยัง manifest.xml



sensor_msgs::Joy msg;
float data[4]={0,0,0,0};

void Callback(const boost::shared_ptr<sensor_msgs::Joy const>& msg)
{ 
  data[0]=msg->buttons[0];  //A
  data[1]=msg->buttons[1];  //B
  data[2]=msg->buttons[2];  //X
  data[3]=msg->buttons[3];  //Y

return;
}

int main(int argc, char** argv)
{   
    ros::init(argc, argv, "test");//ตั้งตาทที่ตั้งไว้ที่ cmake
    ros::NodeHandle n;//ประกาศโนด
    ros::Subscriber sub = n.subscribe("joy",1000, &Callback);//ตายตัว
    ros::Publisher chatter_pub  = n.advertise<geometry_msgs::PoseStamped>("move_base_simple/goal",1000);  
    ros::Rate rate(5);//กำหนดค่าในการหลับ มีพื่อเครื่องทำงานไม่หนัก
    geometry_msgs::PoseStamped goal;
    
  while(ros::ok()){  //ถ้าROS พร้อม
   if(data[0]==1){goal.header.frame_id="/map"; goal.pose.position.x=1.5; goal.pose.position.y=-2.5; goal.pose.orientation.z=-0.0; goal.pose.orientation.w=1; chatter_pub.publish(goal);}

 
    ros::spinOnce();
    rate.sleep();
}	

  return 0;
}


/*
   
if(data[0]==1){system("/home/turtlebot/ros/object/bin/surf2");   printf("gggg");}

if(data[1]==1){exit(0);}
*/
