#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <math.h>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>

float scale_linear_ = 0.15;
float scale_angular_ = 0.2;

float x;
float y;
float z;

geometry_msgs::Twist vel;



void Callback(const sensor_msgs::Joy& msg)
{ 
	if(msg.buttons[4]==1)
	{
		scale_linear_=scale_linear_+0.02;
		scale_angular_=scale_angular_+0.02;
	}

	if(msg.buttons[5]==1)
	{
		scale_linear_=scale_linear_-0.02;
		scale_angular_=scale_angular_-0.02;
	}
	vel.linear.x = msg.axes[1] * scale_linear_;
	vel.linear.y = msg.axes[0] * scale_linear_;
	vel.angular.z = msg.axes[6] * scale_angular_;
}

int main(int argc, char** argv)
{   
    ros::init(argc, argv, "joy_to_bot");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("joy",100, &Callback); 
    ros::Publisher chatter_pub  = n.advertise<geometry_msgs::Twist>("cmd_vel",100);
    ros::Rate rate(100);
    while(ros::ok())
{ 
    chatter_pub.publish( vel );
    ros::spinOnce();
    rate.sleep();
}	
  return 0;
}

