#include <ros/ros.h>
#include <math.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int64.h>

#define PI 3.1415926535897932384626433
#define L1 0.245918  
#define L2 0.137
#define R 0.076

float velocity_x;
float velocity_y;
float angular_z;
float omegaWheel1;
float omegaWheel2;
float omegaWheel3;
float omegaWheel4;
float test;

void Callback(const boost::shared_ptr<geometry_msgs::Twist const>& msg)
{ 
   velocity_x=msg->linear.x;
   velocity_y=msg->linear.y;
	//if(msg->angular.z>0.3){angular_z=0.3;}
	//if(msg->angular.z<-0.3){angular_z=-0.3;}
        //if(msg->angular.z<0.3&&msg->angular.z>-0.3)
	//{
	angular_z=msg->angular.z;
	//}
}


int main(int argc, char** argv)
{   
    ros::init(argc, argv, "sunny");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("cmd_vel",1000, &Callback);
    ros::Publisher chatter_pub1  = n.advertise<std_msgs::Float64>("wheel1/command",1000);  
    ros::Publisher chatter_pub2  = n.advertise<std_msgs::Float64>("wheel2/command",1000);
    ros::Publisher chatter_pub3  = n.advertise<std_msgs::Float64>("wheel3/command",1000);  
    ros::Publisher chatter_pub4  = n.advertise<std_msgs::Float64>("wheel4/command",1000);
    ros::Rate rate(100);
    std_msgs::Float64 omegaWheel1;
    std_msgs::Float64 omegaWheel2;
    std_msgs::Float64 omegaWheel3;
    std_msgs::Float64 omegaWheel4;

    
  while(ros::ok())
{  
    omegaWheel1.data = -0.667*(velocity_x - velocity_y - (angular_z*(L1+L2)))/R/(2.4);
    omegaWheel2.data = 0.667*(velocity_x + velocity_y + (angular_z*(L1+L2)))/R/(2.4);
    omegaWheel3.data = -0.667*(velocity_x + velocity_y - (angular_z*(L1+L2)))/R/(2.4);
    omegaWheel4.data = 0.667*(velocity_x - velocity_y + (angular_z*(L1+L2)))/R/(2.4);
    chatter_pub1.publish(omegaWheel1);
    chatter_pub2.publish(omegaWheel2);
    chatter_pub3.publish(omegaWheel3);
    chatter_pub4.publish(omegaWheel4);
    test=omegaWheel1.data;
    ROS_INFO("%f",test);
    ros::spinOnce();
    rate.sleep();
}	

  return 0;
}
