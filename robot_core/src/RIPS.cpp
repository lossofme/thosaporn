#include <ros/ros.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <sstream> 
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Int64.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <move_base_msgs/MoveBaseActionFeedback.h>
#include <string.h>
#include <sensor_msgs/LaserScan.h>

int order;
int sec;
int start=0;
float positionx;
float positiony;
float angularw;
int status=0;

void Callback(const std_msgs::Int64& msg)
{ 
	order = msg.data;
	if(msg.data == 69)sec=4;
}

void CallbackScan(const sensor_msgs::LaserScanConstPtr& msg)
{ 
	if(start == 0)
		{
		if(msg->ranges[255] > 0.7 || msg->ranges[255] == 0){sec=1; start=1;}
		}
	ROS_INFO("sec %d",sec);
}

void CallbackNavi(const move_base_msgs::MoveBaseActionFeedbackConstPtr& msg)
{ 
	positionx =msg->feedback.base_position.pose.position.x;
	positiony =msg->feedback.base_position.pose.position.y;
	angularw =msg->feedback.base_position.pose.orientation.w;
	ROS_INFO("%f",positionx);

}



int main(int argc, char** argv)
{   
    ros::init(argc, argv, "RIPS");
    ros::NodeHandle n;
    ros::Subscriber order_sub = n.subscribe("order",1000, &Callback);
    ros::Subscriber scan_sub = n.subscribe("scan",1000, &CallbackScan);
    ros::Subscriber navi_sub = n.subscribe("move_base/feedback",1000, &CallbackNavi);
    ros::Publisher navi_pub  = n.advertise<geometry_msgs::PoseStamped>("move_base_simple/goal",1000);
    ros::Publisher velocity_pub  = n.advertise<geometry_msgs::Twist>("cmd_vel",1000);     
    ros::Publisher speak_pub  = n.advertise<std_msgs::Int64>("speak_core",1000); 
    ros::Publisher arm_pub  = n.advertise<std_msgs::Int64>("arm_core",1000); 
    ros::Rate rate(5);
    geometry_msgs::Twist velocity;
    geometry_msgs::PoseStamped goal;
  while(ros::ok()){

   if(sec==1)  
   {
   ros::Duration(1).sleep();
   velocity.linear.x=0.2;
   velocity.linear.y=0;
   velocity.angular.z=0;
   velocity_pub.publish(velocity);
   ros::Duration(4).sleep(); 
   velocity.linear.x=0;
   velocity.linear.y=0;
   velocity.angular.z=0;
   velocity_pub.publish(velocity);
   sec=2;
   }

  if(sec==2)
  {
  goal.header.frame_id="/map"; 
  goal.pose.position.x=2.05; 
  goal.pose.position.y=2.925; 
  goal.pose.orientation.z=-0.857; 
  goal.pose.orientation.w=0.515; 
  navi_pub.publish(goal);
  sec=99;
  }
  
  if(sec==4)
  {
  goal.header.frame_id="/map"; 
  goal.pose.position.x=0.7836; 
  goal.pose.position.y=-4.3906; 
  goal.pose.orientation.z=0.4888; 
  goal.pose.orientation.w=0.872; 
  navi_pub.publish(goal);
  sec=99;
  }



  ros::spinOnce();
  rate.sleep();
}	
  return 0;
}


