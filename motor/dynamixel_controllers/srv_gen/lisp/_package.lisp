(cl:defpackage dynamixel_controllers-srv
  (:use )
  (:export
   "SETCOMPLIANCESLOPE"
   "<SETCOMPLIANCESLOPE-REQUEST>"
   "SETCOMPLIANCESLOPE-REQUEST"
   "<SETCOMPLIANCESLOPE-RESPONSE>"
   "SETCOMPLIANCESLOPE-RESPONSE"
   "TORQUEENABLE"
   "<TORQUEENABLE-REQUEST>"
   "TORQUEENABLE-REQUEST"
   "<TORQUEENABLE-RESPONSE>"
   "TORQUEENABLE-RESPONSE"
   "RESTARTCONTROLLER"
   "<RESTARTCONTROLLER-REQUEST>"
   "RESTARTCONTROLLER-REQUEST"
   "<RESTARTCONTROLLER-RESPONSE>"
   "RESTARTCONTROLLER-RESPONSE"
   "SETCOMPLIANCEMARGIN"
   "<SETCOMPLIANCEMARGIN-REQUEST>"
   "SETCOMPLIANCEMARGIN-REQUEST"
   "<SETCOMPLIANCEMARGIN-RESPONSE>"
   "SETCOMPLIANCEMARGIN-RESPONSE"
   "STOPCONTROLLER"
   "<STOPCONTROLLER-REQUEST>"
   "STOPCONTROLLER-REQUEST"
   "<STOPCONTROLLER-RESPONSE>"
   "STOPCONTROLLER-RESPONSE"
   "SETSPEED"
   "<SETSPEED-REQUEST>"
   "SETSPEED-REQUEST"
   "<SETSPEED-RESPONSE>"
   "SETSPEED-RESPONSE"
   "STARTCONTROLLER"
   "<STARTCONTROLLER-REQUEST>"
   "STARTCONTROLLER-REQUEST"
   "<STARTCONTROLLER-RESPONSE>"
   "STARTCONTROLLER-RESPONSE"
   "SETTORQUELIMIT"
   "<SETTORQUELIMIT-REQUEST>"
   "SETTORQUELIMIT-REQUEST"
   "<SETTORQUELIMIT-RESPONSE>"
   "SETTORQUELIMIT-RESPONSE"
   "SETCOMPLIANCEPUNCH"
   "<SETCOMPLIANCEPUNCH-REQUEST>"
   "SETCOMPLIANCEPUNCH-REQUEST"
   "<SETCOMPLIANCEPUNCH-RESPONSE>"
   "SETCOMPLIANCEPUNCH-RESPONSE"
  ))

